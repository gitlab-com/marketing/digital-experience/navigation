## What does this affect? Check all that apply.
- [ ] Navigation
  - [ ] Mobile
  - [ ] Desktop
- [ ] Footer
- [ ] Other (please explain)

## Goal
Please describe the bug or update involved in this request. Feel free to include screenshots, recordings, or browser information in order to reproduce.

## DCI
[DRI, Consulted, Informed](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#dri-consulted-informed-dci)

- [ ] DRI: `GitLab Handle`
- [ ] Consulted: `GitLab Handle`
- [ ] Informed: `Everyone`

/label ~"dex-status::triage"
