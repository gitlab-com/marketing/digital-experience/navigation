export const checkIfUrlExists = (url: string) => {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then((response) => resolve(response.status === 200))
      .catch((error) => reject(error));
  });
};

export const redirectToUrl = (url: string, blank: boolean) => {
  if (blank) {
    window.open(url, "_blank");
  } else {
    window.location.href = url;
  }
};
