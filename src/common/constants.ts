import { Language } from "@/services/language.service";

export enum FreeTrialVariant {
  PRIMARY = "primary",
  ACCENT = "accent",
  GLEAM = "gleam",
  STEEL = "steel",
}

export const LANG_OPTIONS: readonly Language[] = [
  {
    label: "English",
    code: "en",
    value: "en-us",
    path: "", // In our site the default language is English and does not have a prefix path
    default: true,
    langLabel: "Language",
  },
  {
    label: "Deutsch",
    code: "de",
    value: "de-de",
    path: "/de-de",
    langLabel: "Sprache",
  },
  {
    label: "Español",
    code: "es",
    value: "es",
    path: "/es",
    langLabel: "Idioma",
  },
  {
    label: "Français",
    code: "fr",
    value: "fr-fr",
    path: "/fr-fr",
    langLabel: "Langue",
  },
  {
    label: "Italiano",
    code: "it",
    value: "it-it",
    path: "/it-it",
    langLabel: "Lingua",
  },
  {
    label: "日本語",
    code: "ja",
    value: "ja-jp",
    path: "/ja-jp",
    langLabel: "言語",
  },
  {
    label: "Português",
    code: "pt",
    value: "pt-br",
    path: "/pt-br",
    langLabel: "Idioma",
  },
];
