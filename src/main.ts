import Vue from "vue";
import Components from "slippers-ui";
import App from "./App.vue";

// Register Slipper components with Vue
Object.keys(Components).forEach((name) => {
  Vue.component(name, Components[name]);
});

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
