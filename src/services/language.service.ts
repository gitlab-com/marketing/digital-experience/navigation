import { LANG_OPTIONS } from "@/common/constants";
import { checkIfUrlExists, redirectToUrl } from "@/common/utils";

export interface Language {
  label: string;
  code: string;
  value: string;
  path: string;
  langLabel: string;
  default?: boolean;
}

/**
 * Detects the languages available for a given page URL by checking the existence of its localized URL's.
 *
 * @param {string} baseUrl - The base URL for the page translations.
 * @param {string} path - The path of the page to check translations for.
 * @returns {Promise<Array>} A promise that resolves with an array of the available languages for the page.
 */
export const getPageAvailableLanguages = async (
  baseUrl: string,
  path: string
): Promise<any[]> => {
  const currentLanguage = LANG_OPTIONS.find(
    (language) => language.path !== "" && path.includes(language.path)
  );

  const localizationPromises = LANG_OPTIONS.map((language) => {
    let url = `${baseUrl}${language.path}${path}`;

    if (currentLanguage) {
      url = url.replace(currentLanguage.path, "");
    }
    return checkIfUrlExists(url);
  });

  const response = await Promise.all(localizationPromises);
  // The Promise.all method strictly keeps the same order, allowing the use of response[index]
  const availableLanguages = LANG_OPTIONS.filter((_, index) => response[index]);

  if (availableLanguages.length <= 1) {
    console.log(
      `٩◔̯◔۶ i18n: ${window.location.pathname} page is not localized, 404 errors raised ٩◔̯◔۶`
    );
  }

  return availableLanguages;
};

export const getUnlocalizedPath = (path: string) => {
  const pathLang = LANG_OPTIONS.filter((lang) => !lang.default).find((lang) =>
    path.includes(lang.path)
  );
  return pathLang ? path.replace(pathLang.path, "") : path;
};

/**
 * This method redirects the user to the preferred language.
 * It checks if the next page exists for the selected language in order to redirect the user to an existing page.
 * If the localized page does not exist, the redirection will be done to the same un-localized path.
 * @param path Un-localized path
 * @param event Event from the clicking element
 */
export const redirectToLocalizedLink = (path: string, event) => {
  const isCmdTyped = event.ctrlKey || event.metaKey;

  if (path.startsWith("/")) {
    const flatLang = localStorage.getItem("lang");
    let selectedLang: Language;

    if (flatLang) {
      selectedLang = JSON.parse(flatLang);
      const selectedLangPath = `${selectedLang.path}${path}`;
      checkIfUrlExists(selectedLangPath).then((exists) =>
        redirectToUrl(exists ? selectedLangPath : path, isCmdTyped)
      );
    } else {
      redirectToUrl(path, isCmdTyped);
    }
  } else {
    redirectToUrl(path, isCmdTyped);
  }
};
