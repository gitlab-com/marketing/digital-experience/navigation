import SlpNavigation from "./components/Navigation/Navigation.vue";
import SlpFooter from "./components/Footer/Footer.vue";
import SlpMinimizedNavigation from "./components/Navigation/minimized-navigation.vue";
import SlpMinimizedFooter from "./components/Footer/minimal-footer.vue";

export { SlpNavigation, SlpFooter, SlpMinimizedNavigation, SlpMinimizedFooter };

export default {
  SlpNavigation,
  SlpFooter,
  SlpMinimizedNavigation,
  SlpMinimizedFooter,
};
