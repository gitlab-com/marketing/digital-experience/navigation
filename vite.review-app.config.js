import { fileURLToPath, URL } from "node:url";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue2";

export default defineConfig({
  base: "",
  plugins: [vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
      "@gitlab/fonts": fileURLToPath(
        new URL("./node_modules/@gitlab/fonts", import.meta.url)
      ),
    },
  },
  assetsInclude: ["**/*.woff2"],
  build: {
    rollupOptions: {
      input: {
        main: fileURLToPath(new URL("./index.html", import.meta.url)),
      },
    },
    assetsInlineLimit: 0,
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData:
          '@import "src/styles/all.scss"; @import "src/styles/fonts.scss";',
      },
    },
  },
});
