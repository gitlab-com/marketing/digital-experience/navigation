import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue2";
import { visualizer } from "rollup-plugin-visualizer";
import { resolve } from "pathe";

// https://vitejs.dev/config/

export default defineConfig({
  base: "./",
  plugins: [vue(), visualizer()],
  resolve: {
    alias: {
      "@": "/src",
    },
  },
  build: {
    outDir: resolve(__dirname, "lib"),
    lib: {
      entry: resolve(__dirname, "src/index.ts"),
      name: "be-navigation",
      fileName: (format) => `be-navigation.${format}.js`,
      formats: ["es", "umd"],
    },
    rollupOptions: {
      external: ["slippers-ui", "vue"],
      output: {
        globals: {
          vue: "Vue",
          "slippers-ui": "Components",
        },
        exports: "named",
      },
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "src/styles/all.scss";',
      },
    },
  },
});
