# Buyer Experience Navigation

**Buyer Experience Navigation** (be-navigation) contains a modular navigation for [about.gitlab.com](https://about.gitlab.com).
- be-navigation is a Vite library that features our implementation of GitLab's marketing website's navigation.
- be-navigation is a collection of navigation components. Currently housing the main navigation and the footer.
- be-navigation uses GitLab's [Slippers UI](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui) design system.

## Getting Started
### Dependencies
- Node - Check `.nvmrc`, `.tool-versions`, `.npmrc` for the current node version.

### Local development
Start app: ```yarn dev```
Lint: ```yarn lint```
Serve built app: ```yarn preview```

### Transitioning from Vue CLI 2

In previous versions, we had used Vue CLI, so make sure to update `yarn install` dependencies if you had used this repo before the transition to Vite. Another big change we noticed was the removal of the `scoped` attribute in Single File Components. This was leading to large CSS payloads. We should aim to keep the contents of `/dist` under ~250kb to make sure that it is production optimized. 

One consequence of this transition was that we broke Review Apps for this repo. We should configure that as soon as we have bandwidth for that. 

### Updating NPM package and releasing a new version
1. Create a new issue titled `Navigation release: Version x.x.xx` and include links to the MRs and Issues included in this release
1. Create a new branch from `main`, with all changes
1. Increment the version number in `package.json`
2. Build /dist folder - ```yarn build```
3. Publish to npm - ```npm publish``` (Note: You may need to login with your npm credentials using ```npm login```)
4. Merge changes to `origin/main`
5. Once the package has been published it's now ready to be updated in the consuming repositories (Example: www, Buyer Experience). Include links to those MRs in your Navigation Release Issue 
6. Close the Navigation Release Issue

## Installation
be-navigation is available as an [npm package](https://www.npmjs.com/package/be-navigation).

    // with npm
    npm install be-navigation

    // with yarn
    yarn add be-navigation
