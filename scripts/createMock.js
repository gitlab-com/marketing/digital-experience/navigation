const contentful = require("contentful");
const fs = require("fs");

require("dotenv").config();

const navigationConfig = {
  accessToken: process.env.USE_CONTENTFUL_PREVIEW_API
    ? process.env.CTF_PREVIEW_ACCESS_TOKEN
    : process.env.CTF_CDA_ACCESS_TOKEN,
  space: process.env.CTF_SPACE_ID,
};

if (process.env.USE_CONTENTFUL_PREVIEW_API) {
  navigationConfig.host = "preview.contentful.com";
}

const navClient = contentful.createClient(navigationConfig);

const ctfConfig = {
  "sys.id": process.env.CTF_NAV_ENTRY_ID,
  order: "sys.id",
  include: 10,
};

navClient
  .getEntries(ctfConfig)
  .then((response) =>
    fs.writeFileSync("navMock.json", JSON.stringify(response))
  );
