# Swiftype Site-Search Integration

The search feature works by consuming the site-search API from GitLab's Swiftype instance that can be found in 
https://app.swiftype.com/home.

More deep documentation can be found in https://swiftype.com/documentation/site-search/site_search

## Main search

To make any search the next public endpoint from Swiftype is used to make the search within any `Engine`:

`POST https://search-api.swiftype.com/api/v1/public/engines/search`

In Swiftype's instance from GitLab there are 3 different engines `(Marketing, Handbook, Blog)` which are separated from each other,
that means, each search is independent and there is no way to search by the three of them at once.

These are the main attributes that must be in the body in order to get a successful response

| Attribute | Description |
|-----------|-------------|
| engine_key | Public key which is different for each Engine and only one can be used per request |
| q | Search term |

More info in https://swiftype.com/documentation/site-search/searching

## Pagination

For the pagination these are the required attributes:

| Attribute | Description | Default Value |
|-----------|-------------|---------------|
| page | The current page that will be requested | 1 |
| per_page | Number of items per page | 20 |

More info in https://swiftype.com/documentation/site-search/searching/pagination

## Query Suggestions / Spelling

For the suggestion feature to work, it's required to use the `spelling` attribute with the `always` value,
more info about this in https://swiftype.com/documentation/site-search/searching/spelling.

## Sorting

The sorting feature works by declaring which attribute and direction should be sorted the data,
for GitLab's search the `page` object is the one that is sorted, the next attributes are the ones 
that are available to do the sorting in the UI.

   - update_at (Last updated)
   - popularity (Most popular)
   - _score (Suggested)

## Final Example

A final example request body is like this:

```json
{
      "engine_key": "6meAsJr1HTFB8FoyaYAv",
      "q": "ci cd",
      "page": 1,
      "per_page": 10,
      "sort_field": {
        "page": "updated_at"
      },
      "sort_direction": {
        "page": "desc"
      }
}
```